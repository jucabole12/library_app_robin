import 'package:flutter/material.dart';
import 'package:library_app_robin/presentation/utils/routes/routes.dart';
import 'package:library_app_robin/presentation/utils/routes/routes_name.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'RobinBook',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: RoutesName.splash,
      routes: Routes.loadRoutes,
    );
  }
}
