import 'dart:async';

import 'package:flutter/material.dart';
import 'package:library_app_robin/data/models/book.model.dart';
import 'package:library_app_robin/data/services/book.service.dart';
import 'package:library_app_robin/presentation/utils/bloc/bloc_field.dart';
import 'package:library_app_robin/presentation/utils/bloc/bloc_provider.dart';

class HomeBloc implements BlocBase {
  static HomeBloc _instance;
  factory HomeBloc() {
    if (_instance == null) {
      _instance = HomeBloc._internal();
      _instance.init();
    }
    return _instance;
  }
  HomeBloc._internal();

  void init() => searchBookByTitleOrAuthor.stream.listen((event) {
        onSearchChangedDebounce(event);
      });

  BookService service = BookService();

  final noMatchesFinded = FieldBlocGeneric<bool>(defaultValue: false);
  final searchBookByTitleOrAuthor = FieldBlocGeneric<String>(defaultValue: '');
  final indexPage = FieldBlocGeneric<int>(defaultValue: 0);
  final isLoadingSearch = FieldBlocGeneric<bool>(defaultValue: false);
  final bookList =
      FieldBlocGeneric<List<BookModel>>(defaultValue: <BookModel>[]);
  final favoriteBooks =
      FieldBlocGeneric<List<BookModel>>(defaultValue: <BookModel>[]);
  final searchActive = FieldBlocGeneric<bool>(defaultValue: false);

  Timer _debounce;
  void onSearchChangedDebounce(String query) {
    if (_debounce?.isActive ?? false) _debounce.cancel();
    _debounce = Timer(const Duration(seconds: 1), () {
      isLoadingSearch.sink(true);
      print('SEARH =======================================> :::: $query');
      service.searchBooks(query, this);
    });
  }

  void setFavoriteBook(BookModel item, BuildContext context) {
    final books = favoriteBooks.value;
    final findBook = books.firstWhere((element) => element == item, orElse: () {
      return null;
    });
    if (findBook != null) {
      books.remove(item);
      favoriteBooks.sink(books);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Libro eliminado de favoritos'),
      ));
    } else {
      books.add(item);
      favoriteBooks.sink(books);
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Libro guardado en favoritos'),
      ));
    }
    // print('LISTAAAA ' + books.toString());
  }

  @override
  void dispose() {
    indexPage.dispose();
    service.dispose();
    _debounce.cancel();
    noMatchesFinded.dispose();
    searchBookByTitleOrAuthor.dispose();
    isLoadingSearch.dispose();
    bookList.dispose();
    favoriteBooks.dispose();
    searchActive.dispose();
  }
}
