import 'package:flutter/material.dart';
import 'package:library_app_robin/presentation/utils/bloc/bloc_provider.dart';
import 'package:library_app_robin/presentation/utils/routes/routes_name.dart';

class SplashBloc implements BlocBase {
  static SplashBloc _instance;
  factory SplashBloc() {
    _instance ??= SplashBloc._internal();
    return _instance;
  }
  SplashBloc._internal();

  void init(BuildContext context) {
    Future.delayed(Duration(seconds: 2),
        () => Navigator.of(context).pushReplacementNamed(RoutesName.home));
  }

  @override
  void dispose() {}
}
