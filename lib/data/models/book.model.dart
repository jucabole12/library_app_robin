class BookModel {
  String title;
  String authors;
  String publicationYear;
  String idCover;
  bool isFavorite;

  BookModel({
    this.title,
    this.authors,
    this.publicationYear,
    this.idCover,
    this.isFavorite = false,
  });

  Map<String, dynamic> toJSON() => {
        'title': title,
        'author_name': authors,
        'first_publish_year': publicationYear,
        'idCover': idCover,
      };

  BookModel fromJson(Map<String, dynamic> json) {
    return BookModel(
      title: json['title'].toString(),
      authors: json['author_name'] != null
          ? json['author_name'].cast<String>().length > 1
              ? json['author_name'].cast<String>().join(', ')
              : json['author_name'].cast<String>()[0]
          : '',
      publicationYear: json['first_publish_year'] != null
          ? json['first_publish_year'].toString()
          : '',
      idCover: json['cover_i'] != null ? json['cover_i'].toString() : '',
    );
  }
}
