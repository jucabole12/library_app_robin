import 'package:dio/dio.dart';
import 'package:library_app_robin/data/blocs/home.bloc.dart';
import 'package:library_app_robin/data/models/book.model.dart';
import 'package:library_app_robin/presentation/utils/bloc/bloc_provider.dart';

class BookService implements ServiceBase {
  static BookService _instance;

  factory BookService() {
    _instance ??= BookService._internal();
    return _instance;
  }
  BookService._internal();

  final String url = 'http://openlibrary.org/search.json?';
  final Dio dio = Dio();

  Future<void> searchBooks(String query, HomeBloc bloc) async {
    if (!['', null].contains(query) && query.length > 3) {
      bloc.bookList.sink(<BookModel>[]);
      await queryToFindBooks(query, bloc);
    }
    if (['', null].contains(query)) {
      bloc.bookList.sink(<BookModel>[]);
      bloc.noMatchesFinded.sink(false);
      bloc.isLoadingSearch.sink(false);
    }
  }

  Future<List<BookModel>> queryToFindBooks(String title, HomeBloc bloc) async {
    final bookList = <BookModel>[];
    try {
      final res = await dio.get(url, queryParameters: {
        'q': title,
        'mode': 'ebooks',
        'has_fulltext': true
      });
      print((Map<String, dynamic>.from(res.data)['docs']).map((e) {
        bookList.add(BookModel().fromJson(e));
      }));
      bloc.bookList.sink(bookList);
      bloc.isLoadingSearch.sink(false);
      if (bookList.isEmpty) {
        bloc.noMatchesFinded.sink(true);
      } else {
        bloc.noMatchesFinded.sink(false);
      }
      return Future.value(bookList);
    } catch (e) {
      print(e);
      bloc.isLoadingSearch.sink(false);
      return Future.error(e);
    }
  }

  @override
  void dispose() {
    _instance = null;
  }
}
