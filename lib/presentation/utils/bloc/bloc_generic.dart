import 'dart:async';

import 'package:rxdart/rxdart.dart';
import 'bloc_provider.dart';

class Bloc<T> implements BlocBase {
  final controller = BehaviorSubject<T>();
  final T init;

  Bloc({this.init}) {
    if (init != null) {
      sink(init);
    }
  }
  Stream<T> get stream => controller.stream;
  Function(T) get sink => controller.sink.add;
  T get value => controller.value;

  @override
  void dispose() {
    controller.close();
  }
}
