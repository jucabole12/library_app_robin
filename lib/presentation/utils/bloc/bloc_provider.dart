import 'package:flutter/material.dart';

class BlocProvider<T extends BlocBase> extends StatefulWidget {
  BlocProvider({Key key, @required this.child, this.bloc})
      : assert(child != null),
        super(key: key);

  final T bloc;
  final Widget child;

  @override
  _BlocProviderState<T> createState() => _BlocProviderState<T>();

  static T ofBloc<T extends BlocBase>(BuildContext context) {
    final provider = context.findAncestorWidgetOfExactType<BlocProvider<T>>();
    return provider?.bloc;
  }
}

class _BlocProviderState<T> extends State<BlocProvider<BlocBase>> {
  @override
  void dispose() {
    widget.bloc?.dispose();
    print('CLEAN MEMORY ${widget.child.runtimeType.toString()}');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}

abstract class BlocBase {
  void dispose();
}

abstract class ServiceBase {
  void dispose();
}
