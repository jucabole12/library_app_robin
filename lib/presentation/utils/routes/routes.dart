import 'package:flutter/material.dart';
import 'package:library_app_robin/data/blocs/home.bloc.dart';
import 'package:library_app_robin/data/blocs/splash.bloc.dart';
import 'package:library_app_robin/presentation/pages/home/home.page.dart';
import 'package:library_app_robin/presentation/pages/splash/splash.page.dart';
import 'package:library_app_robin/presentation/utils/bloc/bloc_provider.dart';
import 'package:library_app_robin/presentation/utils/routes/routes_name.dart';

class Routes {
  static Map<String, Widget Function(BuildContext)> get loadRoutes => {
        RoutesName.home: (BuildContext context) => BlocProvider<HomeBloc>(
              bloc: HomeBloc(),
              child: HomePage(),
            ),
        RoutesName.splash: (context) => BlocProvider<SplashBloc>(
              bloc: SplashBloc(),
              child: SplashPage(),
            ),
      };
}
