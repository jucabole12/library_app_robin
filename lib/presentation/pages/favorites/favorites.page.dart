import 'package:flutter/material.dart';
import 'package:library_app_robin/data/blocs/home.bloc.dart';
import 'package:library_app_robin/data/models/book.model.dart';
import 'package:library_app_robin/presentation/pages/books/books_list.page.dart';
import 'package:lottie/lottie.dart';

class FavoritesPage extends StatelessWidget {
  const FavoritesPage({this.bloc});

  final HomeBloc bloc;

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return StreamBuilder<List<BookModel>>(
        stream: bloc.favoriteBooks.stream,
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data.isNotEmpty) {
            return Container(
              height: double.infinity,
              child: BooksListPage(
                  bloc: bloc,
                  onTapFavorite: (index) {
                    bloc.setFavoriteBook(
                        bloc.favoriteBooks.value[index], context);
                  },
                  bookList: bloc.favoriteBooks.value),
            );
          } else {
            return SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(top: 50.0),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Lottie.asset(
                        'assets/no_matches.json',
                        width: screenWidth * 0.6,
                      ),
                      SizedBox(height: 10.0),
                      Text(
                        'No hay libros agregados',
                        style: TextStyle(fontSize: 20, color: Colors.grey),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
              ),
            );
          }
        });
  }
}
