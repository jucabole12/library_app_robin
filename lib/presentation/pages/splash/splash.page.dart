import 'package:flutter/material.dart';
import 'package:library_app_robin/data/blocs/splash.bloc.dart';
import 'package:library_app_robin/presentation/utils/bloc/bloc_provider.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.ofBloc<SplashBloc>(context);
    bloc.init(context);
    return Scaffold(
      body: Container(
        color: Color(0xff5546a3),
        child: Center(
          child: Stack(children: [
            Positioned(
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
              child: Container(
                  height: 100,
                  width: 100,
                  child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Color(0xffea5078)))),
            ),
            Padding(
              padding: const EdgeInsets.all(3.0),
              child: CircleAvatar(
                radius: 55,
                backgroundColor: Colors.white,
                child: CircleAvatar(
                  radius: 45,
                  backgroundImage: AssetImage('assets/robin.jpeg'),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
