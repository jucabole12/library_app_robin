import 'package:flutter/material.dart';
import 'package:library_app_robin/data/blocs/home.bloc.dart';
import 'package:library_app_robin/presentation/pages/books/books_list.page.dart';
import 'package:lottie/lottie.dart';

class BooksPage extends StatelessWidget {
  const BooksPage({this.bloc});

  final HomeBloc bloc;

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Container(
      height: double.infinity,
      child: !bloc.noMatchesFinded.value
          ? BooksListPage(
              bloc: bloc,
              onTapFavorite: (index) {
                bloc.setFavoriteBook(bloc.bookList.value[index], context);
              },
              bookList: bloc.bookList.value,
            )
          : SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(top: 50.0),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Lottie.asset(
                        'assets/no_matches.json',
                        width: screenWidth * 0.6,
                      ),
                      SizedBox(height: 10.0),
                      Text(
                        'No se encontraron coincidencias',
                        style: TextStyle(fontSize: 20, color: Colors.grey),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                ),
              ),
            ),
    );
  }
}
