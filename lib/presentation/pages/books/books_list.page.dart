import 'package:flutter/material.dart';
import 'package:library_app_robin/data/blocs/home.bloc.dart';
import 'package:library_app_robin/data/models/book.model.dart';
import 'package:library_app_robin/presentation/widgets/cardBook/card_book.widget.dart';
import 'package:lottie/lottie.dart';

class BooksListPage extends StatelessWidget {
  const BooksListPage({this.bloc, this.onTapFavorite, this.bookList});

  final HomeBloc bloc;
  final Function(int) onTapFavorite;
  final List<BookModel> bookList;

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Container(
      height: double.infinity,
      child: bookList.isNotEmpty
          ? ListView.builder(
              itemCount: bookList.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    showModalBottomSheet(
                        backgroundColor: Colors.transparent,
                        context: context,
                        builder: (context) {
                          return CardBookWidget(
                            urlImage:
                                'http://covers.openlibrary.org/b/id/${bookList[index].idCover}-L.jpg?default=false',
                            title: bookList[index].title,
                            authors: bookList[index].authors,
                            publicationYear: bookList[index].publicationYear,
                            onTapFavorite: () {
                              onTapFavorite(index);
                            },
                            enableDetailView: true,
                            heightImage: 500,
                            widthImage: 100,
                          );
                        });
                  },
                  child: CardBookWidget(
                    urlImage:
                        'http://covers.openlibrary.org/b/id/${bookList[index].idCover}-L.jpg?default=false',
                    title: bookList[index].title,
                    authors: bookList[index].authors,
                    publicationYear: bookList[index].publicationYear,
                    onTapFavorite: () {
                      onTapFavorite(index);
                    },
                  ),
                );
              })
          : SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(top: 20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Lottie.asset(
                      'assets/books.json',
                      width: screenWidth * 0.8,
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      'Filtra el libro que quieras buscar por título o autor',
                      style: TextStyle(fontSize: 20, color: Colors.grey),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
              ),
            ),
    );
  }
}
