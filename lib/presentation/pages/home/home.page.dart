import 'package:flutter/material.dart';
import 'package:library_app_robin/data/blocs/home.bloc.dart';
import 'package:library_app_robin/presentation/pages/books/books.page.dart';
import 'package:library_app_robin/presentation/pages/favorites/favorites.page.dart';
import 'package:library_app_robin/presentation/utils/bloc/bloc_provider.dart';
import 'package:library_app_robin/presentation/widgets/appBar/app_bar.widget.dart';
import 'package:library_app_robin/presentation/widgets/bottomNavigationBar/bottom_navigation_bar.widget.dart';
import 'package:lottie/lottie.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.ofBloc<HomeBloc>(context);
    return Scaffold(
      appBar: AppBarWidget(bloc: bloc),
      body: _createBody(context, bloc),
      bottomNavigationBar: BottomNavigationBarWidget(
        items: [
          Icon(Icons.search, size: 25, color: Colors.white),
          Icon(Icons.list, size: 25, color: Colors.white),
        ],
        onTap: (index) {
          if (index == 1) {
            bloc.searchActive.sink(false);
          } else {
            bloc.searchActive.sink(true);
          }
          bloc.indexPage.sink(index);
        },
        bloc: bloc,
      ),
    );
  }

  Widget _createBody(BuildContext context, HomeBloc bloc) {
    final widgetsNavigationBottom = <Widget>[
      StreamBuilder<bool>(
          stream: bloc.isLoadingSearch.stream,
          builder: (context, snapshot) {
            if (snapshot.hasData && snapshot.data) {
              final screenWidth = MediaQuery.of(context).size.width;
              return SingleChildScrollView(
                child: Center(
                  child: Padding(
                    padding: EdgeInsets.only(top: 20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Lottie.asset(
                          'assets/loading_search.json',
                          width: screenWidth * 0.8,
                        ),
                        Text(
                          'Buscando coincidencias...',
                          style: TextStyle(fontSize: 20, color: Colors.grey),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),
                ),
              );
            }
            return BooksPage(bloc: bloc);
          }),
      FavoritesPage(bloc: bloc),
    ];
    return StreamBuilder(
      stream: bloc.indexPage.stream,
      builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
        return IndexedStack(
          index: snapshot.data,
          children: widgetsNavigationBottom,
        );
      },
    );
  }
}
