import 'package:flutter/material.dart';
import 'package:library_app_robin/presentation/widgets/imageGeneric/image_generic.widget.dart';

class CardBookWidget extends StatelessWidget {
  const CardBookWidget({
    this.onTapFavorite,
    @required this.urlImage,
    @required this.title,
    @required this.authors,
    // @required this.editorial,
    @required this.publicationYear,
    this.enableDetailView = false,
    this.widthImage = 50,
    this.heightImage = 70,
  });

  final Function onTapFavorite;
  final String urlImage;
  final String title;
  final String authors;
  // final String editorial;
  final String publicationYear;
  final bool enableDetailView;
  final double widthImage;
  final double heightImage;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 5, right: 5),
      height: enableDetailView ? 200 : 110,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Card(
          elevation: 6,
          shadowColor: Colors.black54,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          color: Colors.white,
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Stack(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 10.0, right: 10.0),
                      child: ImageGenericWidget(
                        path: urlImage,
                        width: widthImage,
                        height: heightImage,
                      ),
                    ),
                    Positioned(
                      top: 0,
                      right: 0,
                      child: InkWell(
                        onTap: () => onTapFavorite(),
                        child: Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  offset: Offset(0.0, 1.0), //(x,y)
                                  blurRadius: 2.0,
                                ),
                              ],
                            ),
                            child: Icon(
                              Icons.bookmark_outline,
                              color: Colors.grey,
                              // Colors.yellow[700],
                            )),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      title,
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                      overflow:
                          !enableDetailView ? TextOverflow.ellipsis : null,
                      maxLines: !enableDetailView ? 2 : null,
                    ),
                    const SizedBox(height: 5),
                    Text(
                      authors,
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(height: 5),
                    Text(
                      '$publicationYear',
                      style: TextStyle(
                        color: Colors.grey,
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
