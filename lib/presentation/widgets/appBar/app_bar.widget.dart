import 'package:flutter/material.dart';
import 'package:library_app_robin/data/blocs/home.bloc.dart';
import 'package:library_app_robin/presentation/widgets/appBar/app_bar_search.widget.dart';

class AppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  AppBarWidget({this.bloc});

  final HomeBloc bloc;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 3.0,
      title: StreamBuilder<bool>(
          stream: bloc.searchActive.stream,
          builder: (context, snapshot) {
            var listWidgetsToReturn = <Widget>[
              Container(
                padding: EdgeInsets.all(3),
                decoration:
                    BoxDecoration(color: Colors.white, shape: BoxShape.circle),
                child: Image.asset('assets/robin.jpeg', width: 50),
              ),
            ];
            if (snapshot.hasData && snapshot.data) {
              listWidgetsToReturn = <Widget>[];
              listWidgetsToReturn.add(
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(left: 7),
                    height: 40,
                    child: AppBarSearchWidget(
                      stream: bloc.searchBookByTitleOrAuthor.stream,
                      sink: bloc.searchBookByTitleOrAuthor.sink,
                    ),
                  ),
                ),
              );
            }
            return Row(
              children: listWidgetsToReturn,
            );
          }),
      actions: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: 8.0),
          child: StreamBuilder<bool>(
              stream: bloc.searchActive.stream,
              builder: (context, snapshot) {
                var iconToSearch = Icons.search;
                var dataToSet = true;
                if (snapshot.hasData && snapshot.data) {
                  iconToSearch = Icons.close;
                  dataToSet = false;
                }
                return bloc.indexPage.value == 0
                    ? IconButton(
                        icon: Icon(iconToSearch, color: Colors.grey),
                        onPressed: () {
                          bloc.searchActive.sink(dataToSet);
                        },
                      )
                    : Container();
              }),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
