import 'package:flutter/material.dart';

class AppBarSearchWidget extends StatelessWidget {
  const AppBarSearchWidget({@required this.stream, @required this.sink});

  final Stream<String> stream;
  final Function(String) sink;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
        stream: stream,
        builder: (context, snapshot) {
          final controllerSearch = TextEditingController();
          controllerSearch.text = (snapshot.data != null && snapshot.data != '')
              ? snapshot.data
              : '';

          controllerSearch.value = controllerSearch.value.copyWith(
            text: controllerSearch.text,
            selection: TextSelection(
                baseOffset: controllerSearch.text.length,
                extentOffset: controllerSearch.text.length),
            composing: TextRange.empty,
          );

          return TextFormField(
            controller: controllerSearch,
            autofocus: false,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              prefixIcon: IconButton(
                onPressed: () {},
                color: Colors.grey,
                padding: EdgeInsets.all(0.0),
                disabledColor: Colors.grey,
                focusColor: Colors.black,
                icon: Icon(Icons.search, color: Colors.grey),
              ),
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 0.0),
                borderRadius: BorderRadius.all(
                  Radius.circular(20.0),
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 1.0),
                borderRadius: BorderRadius.all(
                  Radius.circular(20.0),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 1.0),
                borderRadius: BorderRadius.all(
                  Radius.circular(20.0),
                ),
              ),
              contentPadding: EdgeInsets.all(8.0), //here your padding
              filled: true,
              hintText: 'Buscar libro',
              fillColor: Colors.white,
              hintStyle: TextStyle(color: Colors.grey, fontSize: 15),
            ),
            style: TextStyle(color: Colors.grey),
            onChanged: (query) {
              if (sink != null) {
                sink(query);
              }
            },
          );
        });
  }
}
