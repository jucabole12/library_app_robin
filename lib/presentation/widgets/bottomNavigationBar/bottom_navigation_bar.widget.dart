import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:library_app_robin/data/blocs/home.bloc.dart';

class BottomNavigationBarWidget extends StatelessWidget {
  BottomNavigationBarWidget({
    @required this.items,
    @required this.onTap,
    @required this.bloc,
  });

  final List<Widget> items;
  final Function(int) onTap;
  final HomeBloc bloc;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<int>(
        stream: bloc.indexPage.stream,
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            return CurvedNavigationBar(
              index: snapshot.data,
              height: 50.0,
              items: items,
              color: Color(0xff5546a3),
              buttonBackgroundColor: Color(0xff5546a3),
              backgroundColor: Colors.white,
              animationCurve: Curves.easeInOut,
              animationDuration: Duration(milliseconds: 500),
              onTap: (index) {
                onTap(index);
              },
            );
          } else {
            return Container();
          }
        });
  }
}
