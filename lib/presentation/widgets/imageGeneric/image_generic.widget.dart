import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:lottie/lottie.dart';

class ImageGenericWidget extends StatelessWidget {
  const ImageGenericWidget({
    @required this.path,
    this.width = 50.0,
    this.height = 70.0,
  });

  final String path;
  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 400,
      width: width,
      child: CachedNetworkImage(
        imageUrl: path,
        imageBuilder: (context, imageProvider) => Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.contain,
            ),
          ),
        ),
        placeholder: (context, url) => Lottie.asset('assets/loading.json'),
      ),
    );
  }
}
