import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http_mock_adapter/http_mock_adapter.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  Dio dioClient;
  DioAdapter dioAdapter;

  group('Books request', () {
    setUp(() {
      dioClient = Dio(BaseOptions(baseUrl: 'http://openlibrary.org'));
      dioAdapter = DioAdapter(dio: dioClient);

      dioClient.httpClientAdapter = dioAdapter;
    });
    test('Search library with title book ', () async {
      dioAdapter.onGet('/search.json?', (request) {
        request.reply(200, null);
      }, queryParameters: {
        'q': 'tom sawyer',
        'mode': 'ebooks',
        'has_fulltext': true,
      });

      final response = await dioClient.get(
        '/search.json?',
        queryParameters: {
          'q': 'tom sawyer',
          'mode': 'ebooks',
          'has_fulltext': true,
        },
      );

      expect(response.statusCode, 200);
    });

    test('Search library with author book ', () async {
      dioAdapter.onGet('/search.json?', (request) {
        request.reply(200, null);
      }, queryParameters: {
        'q': 'mark twain',
        'mode': 'ebooks',
        'has_fulltext': true,
      });

      final response = await dioClient.get(
        '/search.json?',
        queryParameters: {
          'q': 'mark twain',
          'mode': 'ebooks',
          'has_fulltext': true,
        },
      );

      expect(response.statusCode, 200);
    });
  });
}
